# def MyFunction():
#     return "It is a Function"
#
# MyFunction()

# LEGB
# Local
# Enclosed function
# Global variable
# Build in

def Square(x):
    return x**2

result = Square(3)
print(result)

y = lambda x:x**5
print(y(6))
